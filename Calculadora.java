
import java.util.Scanner;

public class Calculadora {

    static Scanner reader;

    public static void main(String[] args) {
        Boolean continuarEjecucion = true;

        while (continuarEjecucion) {
            reader = new Scanner(System.in);

            imprimirMenu();
            String opcionElegida = reader.next();

            InputUsuarioDto inputUsuarioDto;
            switch (opcionElegida) {
                case "a":
                    inputUsuarioDto = obtenerInputNumerosUsuario();
                    System.out.println("El resultado de la suma entre " + inputUsuarioDto.numero1 + " y "
                            + inputUsuarioDto.numero2 + " es: " + suma(inputUsuarioDto.numero1, inputUsuarioDto.numero2));
                    break;
                case "b":
                    inputUsuarioDto = obtenerInputNumerosUsuario();
                    System.out.println("El resultado de la resta entre " + inputUsuarioDto.numero1 + " y "
                            + inputUsuarioDto.numero2 + " es: " + resta(inputUsuarioDto.numero1, inputUsuarioDto.numero2));
                    break;
                case "c":
                    inputUsuarioDto = obtenerInputNumerosUsuario();
                    System.out.println("El resultado de la multiplicacion entre " + inputUsuarioDto.numero1 + " y " +
                            inputUsuarioDto.numero2 + " es: " + multiplicacion(inputUsuarioDto.numero1, inputUsuarioDto.numero2));
                     break;
                case "d":
                    inputUsuarioDto = obtenerInputNumerosUsuario();
                    System.out.println("El resultado de la division entre " + inputUsuarioDto.numero1 + " y "
                            + inputUsuarioDto.numero2 + " es: " + division(inputUsuarioDto.numero1, inputUsuarioDto.numero2));
                    break;
                    case "e":
                    System.out.println("Saliendo del sistema");
                    continuarEjecucion = false;
                    break;
                default:
                    System.out.println("La opcion ingresada es invalida");
                    continue;
            }
        }
        System.exit(0);
    }

    private static void imprimirMenu() {
        System.out.println("Que operacion desea realizar?");
        System.out.println("a - Suma");
        System.out.println("b - Resta");
        System.out.println("c - Multiplicacion");
        System.out.println("d - Division");
        System.out.println("e - Salir");
    }

    public static Double suma(Double numero1, Double numero2) {
        return numero1 + numero2;
    }

    public static Double resta(Double numero1, Double numero2) {
        return numero1 - numero2;
    }
    public static Double multiplicacion(Double numero1, Double numero2) {
        return numero1 * numero2;
    }
    public static Double division(Double numero1, Double numero2) {
        return numero1 / numero2;
    }

    public static InputUsuarioDto obtenerInputNumerosUsuario() {
        InputUsuarioDto inputUsuarioDto = new InputUsuarioDto();
        reader = new Scanner(System.in);
        try {
            System.out.println("Ingresar primer numero");
            inputUsuarioDto.numero1 = reader.nextDouble();
            System.out.println("Ingresar segundo numero");
            inputUsuarioDto.numero2 = reader.nextDouble();
        } catch (Exception e) {
            System.out.println("El valor ingresado no es un numero");
        }
        return inputUsuarioDto;
    }
}
